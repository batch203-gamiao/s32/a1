const http = require("http");

const port = 4000;

const server = http.createServer((request,response)=>{
	// base
	if(request.url == "/" && request.method == "GET"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Welcome to Booking System");
	}
	// profile
	else if(request.url == "/profile" && request.method == "GET"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Welcome to your profile!");
	}
	// courses
	else if(request.url == "/courses" && request.method == "GET"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Here’s our courses available");
	}
	// addcourses
	else if(request.url == "/addcourses" && request.method == "POST"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Add a course to our resources");
	}
	// updatecourses
	else if(request.url == "/updatecourses" && request.method == "PUT"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Update a course to our resources");
	}
	// archivecourses
	else if(request.url == "/archivecourses" && request.method == "DELETE"){
		response.writeHead(200,{"Content-Type":"application/json"});
		response.end("Archive courses to our resources");
	}
});

server.listen(port);

console.log(`Server is running at local host:${port}`);